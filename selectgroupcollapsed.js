(function ($) {
	'use strict';
	
	// ====================
	// Constructeur
	// ====================
	var SelectGrpColp = function (element, options) {
		// Le select natif
		this.element = element;
		// Le select jquery
		this.$element = $(element);
		// Merge des options par defauts avec les options demandées
		this.options = $.extend(true, {}, this.settings, options);
		
		this.init();
	};
	
	
	// ====================
	// Settings
	// ====================
	
	SelectGrpColp.prototype.settings = {
		// =====================================
		// Class de style à appliquer
		// =====================================
		style: {
			// Container
			container: 'selectgrpcolp',
			
			// Select button
			select: 'sgc-select',
			selectText: 'sgc-select-text',
			selectArrow: 'sgc-select-arrow',
			
			// Dropmenu
			dropmenu: 'sgc-dropmenu',
			
			// Level
			level: 'sgc-level',
			
			// Option
			option: 'sgc-option',
			optlabel: 'sgc-option-label',
			
			// Groupe
			optgroup: 'sgc-optgroup',
			optgroupLabel: 'sgc-optgroup-label',
			optgroupLabelArrow: 'sgc-optgroup-label-arrow',
			optgroupLabelArrowDown: 'sgc-optgroup-label-arrow-down',
			optgroupLabelArrowRight: 'sgc-optgroup-label-arrow-right',
			optgroupLabelText: 'sgc-optgroup-label-text',
			
			// Div hide
			hide: 'sgc-hide'
		},
		// =====================================
		// Les class qui servent à identifier un élément
		// =====================================
		tag: {
			// Container
			container: 'tag-selectgrpcolp',
			
			// Select button
			select: 'sgc-tag-select',
			selectText: 'sgc-tag-select-text',
			selectArrow: 'sgc-tag-select-arrow',
			
			// Dropmenu
			dropmenu: 'sgc-tag-dropmenu',
			
			// Level
			level: 'sgc-tag-level',
			
			// Option
			option: 'sgc-tag-option',
			optlabel: 'sgc-tag-option-label',
			
			// Groupe
			optgroup: 'sgc-tag-optgroup',
			optgroupLabel: 'sgc-tag-optgroup-label',
			optgroupLabelArrow: 'sgc-tag-optgroup-label-arrow',
			optgroupLabelArrowDown: 'sgc-tag-optgroup-label-arrow-down',
			optgroupLabelArrowRight: 'sgc-tag-optgroup-label-arrow-right',
			optgroupLabelText: 'sgc-tag-optgroup-label-text',
			
			// Div hide
			hide: 'sgc-tag-hide'
		},
		// =====================================
		// Template des différents éléments
		// =====================================
		template: {
			// Container
			container: '<div class="tag-selectgrpcolp"></div>',
			
			// Select button
			select: '<button type="button" class="sgc-tag-select"></button>',
			selectText: '<div class="sgc-tag-select-text"></div>',
			selectArrow: '<div class="sgc-tag-select-arrow"><img src="images/arrow-down.svg" height="16" width="16" /></div>',
			
			// Dropmenu
			dropmenu: '<div class="sgc-tag-dropmenu"></div>',
			
			// Level
			level: '<ul class="sgc-tag-level"></ul>',
			
			// Option
			option: '<li class="sgc-tag-option"></li>',
			optlabel: '<span class="sgc-tag-option-label"></span>',
			
			// Groupe
			optgroup: '<li class="sgc-tag-optgroup"></li>',
			optgroupLabel: '<div class="sgc-tag-optgroup-label"></div>',
			optgroupLabelArrow: '<span class="sgc-tag-optgroup-label-arrow"></span>',
			optgroupLabelArrowDown: '<img class="sgc-tag-optgroup-label-arrow-down" src="images/arrow-down.svg" height="14" width="14" />',
			optgroupLabelArrowRight: '<img class="sgc-tag-optgroup-label-arrow-right" src="images/arrow-right.svg" height="14" width="14" />',
			optgroupLabelText: '<span class="sgc-tag-optgroup-label-text"></span>'
		},
		// =====================================
		// Paramètres de configuration
		// =====================================
		optgroupHide: true,
		closeOnSelect: true
	};
	
	// ====================
	// Méthodes publics
	// ====================
    
    // === Select ===
	SelectGrpColp.prototype.createSelect = function () {
		var self = this;
		
		this.$select = $(this.settings.template.select)
			.addClass(this.settings.style.select)
			.addClass(this.$element.attr('class'))
			.on('click', function (e) {
				self.toggleDropmenu();
				e.stopPropagation();
			});
		
		$(this.settings.template.selectText).addClass(this.settings.style.selectText)
			.appendTo(this.$select);
		$(this.settings.template.selectArrow).addClass(this.settings.style.selectArrow)
			.appendTo(this.$select);
		
		this.$select.appendTo(this.$container);
		
		return this;
	};
	
	// === Dropmenu ===
	SelectGrpColp.prototype.createDropmenu = function () {
		var self = this;
		
		this.$dropmenu = $(this.settings.template.dropmenu)
			.addClass(this.settings.style.dropmenu)
			.addClass(this.settings.style.hide)
			.on('click', function (e) {
				e.stopPropagation();
			})
			.appendTo(this.$container);
		
		$('html').on('click', function () {
			self.hideDropmenu();
		});
		
		return this;
	};
	SelectGrpColp.prototype.toggleDropmenu = function () {
		this.$dropmenu.toggleClass(this.settings.style.hide);
		
		return this;
	};
    SelectGrpColp.prototype.showDropmenu = function () {
		this.$dropmenu.removeClass(this.settings.style.hide);

		return this;
    };
	SelectGrpColp.prototype.hideDropmenu = function () {
		this.$dropmenu.addClass(this.settings.style.hide);

		return this;
	};
	
	// === Options ===
	SelectGrpColp.prototype.addOption = function (name, value, optgroupLabel) {
		var self, $option, $optionLabel, $divInsert;
		self = this;
		
		$option = $(this.settings.template.option)
			.addClass(this.settings.style.option)
			.attr('data-value', value)
			.on('click', function () {
				self.selectOption(this);
			});
		
		this.addOptionLabel(name, $option);
		
		if (optgroupLabel)
			$divInsert = this.$dropmenu.find('.' + this.settings.tag.optgroup + '[data-optgroup-label="' + optgroupLabel + '"]>.' + this.settings.tag.level);
		else
			$divInsert = this.$dropmenu.children('.' + this.settings.tag.level);
		
		$divInsert.append($option);
		
		return this;
	};
	SelectGrpColp.prototype.addOptionLabel = function (name, $option) {
		var $optionLabel = $(this.settings.template.optlabel)
			.addClass(this.settings.style.optlabel)
			.append(name)
			.appendTo($option);
		
		return this;
	};
	SelectGrpColp.prototype.selectOption = function (option, fireElementTrigger) {
		var $option = $(option);
		if (typeof(fireElementTrigger) === 'undefined') fireElementTrigger = true;
		
		
		this.$select.attr('data-value', $option.data('value'))
			.children('.' + this.settings.tag.selectText)
			.text($option.first('.' + this.settings.tag.optlabel).text());
		
		if (fireElementTrigger) {
			this.$element.val($option.data('value'))
				.trigger('change');
		}
		
		if (this.settings.closeOnSelect)
			this.hideDropmenu();
		
		return this;
	};
	SelectGrpColp.prototype.unselectOption = function (fireElementTrigger) {
		if (typeof(fireElementTrigger) === 'undefined') fireElementTrigger = true;
		
		this.$select
			.attr('data-value', null)
			.first('.' + this.settings.tag.selectText)
			.empty();
		if (fireElementTrigger) {
			this.$element.val(null)
				.trigger('change');
		}
	};
	
	// === Optgroup ===
	SelectGrpColp.prototype.addOptgroup = function (label) {		
		var $optgroup, $optgroupLabel, $optgroupLabelArrow, $optgroupLevel, self;
		self = this;
		
		$optgroup = $(this.settings.template.optgroup)
			.addClass(this.settings.style.optgroup)
			.attr('data-optgroup-label', label);
		
		// Création du label
		$optgroupLabel = $(this.settings.template.optgroupLabel)
			.addClass(this.settings.style.optgroupLabel)
			.on('click', function () {
				self.toggleOptgroup(label);
			});
		// arrow du label
		$optgroupLabelArrow = $(this.settings.template.optgroupLabelArrow).addClass(this.settings.style.optgroupLabelArrow);
		
		// Création du level
		$optgroupLevel = $(this.settings.template.level)
			.addClass(this.settings.style.level);
		
		// Si les groupes sont cachés
		if (this.settings.optgroupHide) {
			$optgroupLevel.addClass(this.settings.style.hide);
			$(this.settings.template.optgroupLabelArrowRight).addClass(this.settings.style.optgroupLabelArrowRight)
				.appendTo($optgroupLabelArrow);
		}
		else {
			$(this.settings.template.optgroupLabelArrowDown).addClass(this.settings.style.optgroupLabelArrowDown)
				.appendTo($optgroupLabelArrow);
		}
		
		$optgroupLabelArrow.appendTo($optgroupLabel);
		$(this.settings.template.optgroupLabelText).addClass(this.settings.style.optgroupLabelText)
			.text(label)
			.appendTo($optgroupLabel);
		$optgroupLabel.appendTo($optgroup);
		$optgroupLevel.appendTo($optgroup);
		
		
		this.$dropmenu
			.children('.' + this.settings.tag.level)
			.append($optgroup);
		
		return this;
	};
	SelectGrpColp.prototype.toggleOptgroup = function (label) {
		var $optgroup, $optgroupLevel, $optgroupLabel;
		
		$optgroup = this.$dropmenu
			.find('.' + this.settings.tag.optgroup + '[data-optgroup-label="' + label + '"]');
		$optgroupLevel = $optgroup.children('.' + this.settings.tag.level);
		if ($optgroupLevel.is(':visible'))
			this.hideOptgroup(label);
		else
			this.showOptgroup(label);
		
		return this;
	};
    SelectGrpColp.prototype.showOptgroup = function (label) {
		var $optgroup = this.$dropmenu.find('.' + this.settings.tag.optgroup + '[data-optgroup-label="' + label + '"]');
		$optgroup.children('.' + this.settings.tag.level)
			.removeClass(this.settings.style.hide);
		
		$optgroup.find('.' + this.settings.tag.optgroupLabelArrow).html(
			$(this.settings.template.optgroupLabelArrowDown)
				.addClass(this.settings.style.optgroupLabelArrowDown)
		);

		return this;
    };
	SelectGrpColp.prototype.hideOptgroup = function (label) {
		var $optgroup = this.$dropmenu.find('.' + this.settings.tag.optgroup + '[data-optgroup-label="' + label + '"]');
		$optgroup.children('.' + this.settings.tag.level)
			.addClass(this.settings.style.hide);
		
		$optgroup.find('.' + this.settings.tag.optgroupLabelArrow).html(
			$(this.settings.template.optgroupLabelArrowRight)
				.addClass(this.settings.style.optgroupLabelArrowRight)
		);

		return this;
	};
	
	// === Général ===
	SelectGrpColp.prototype.buildSelect = function () {
		var self = this;
		
		this.$element.hide();
		// Création du container
		this.$container = $(this.settings.template.container)
			.addClass(this.settings.style.container)
			.insertAfter(this.$element);
		
		// Création du select button		
		this.createSelect();
		
		// Création du menu
		this.createDropmenu();
		
		return this;
	};
	SelectGrpColp.prototype.updateFromSelect = function () {
		var self, $root;
		
		self = this;
		
		if (!this.$element.val())
			this.unselectOption(false);
		this.$dropmenu.empty();
		$root = $(this.settings.template.level)
			.addClass(this.settings.style.level)
			.appendTo(this.$dropmenu);
		
		this.$element.children().each(function () {
			var $lvl1 = $(this);
			
			// Options de premier niveau
			if ($lvl1.prop('tagName') === 'OPTION') {
				self.addOption($lvl1.text(), $lvl1.val());
				if ($lvl1.is(':selected'))
					self.selectOption($lvl1, false);
			}
			// Groupe
			else if ($lvl1.prop('tagName') === 'OPTGROUP') {
				var optgroupLabel = $lvl1.attr('label');
				self.addOptgroup(optgroupLabel);
				
				// Options de seconds niveau
				$lvl1.children().each(function () {
					var $lvl2 = $(this);
					if ($lvl2.prop('tagName') === 'OPTION') {
						self.addOption($lvl2.text(), $lvl2.val(), optgroupLabel);
						if ($lvl2.prop('selected') == true)
							self.selectOption($lvl2, false);
					}
				});
			}
		});
		
		return this;
	};
	SelectGrpColp.prototype.init = function () {
		var self = this;
		
		this.buildSelect();
		this.updateFromSelect();
		
		this.$element.on('update', function () {
			self.updateFromSelect();
		});
		
		return this;
	};
	
	
	// ====================
	// Init JQuery
	// ====================
		
	$.fn.selectGrpColp = function (options) {
		this.each(function () {
			var $this, data;
			
			$this = $(this);

            // destroy
            if (options === 'destroy') {
                $this.data('selectGrpColp', false);
                return;
            }

            data = $this.data('selectGrpColp');

            // init
            if (!data) {
                $this.data('selectGrpColp', (data = new SelectGrpColp(this, options)));
                return;
            }
		});
		
		return this.first().data('selectGrpColp');
	};
	
})(window.jQuery);